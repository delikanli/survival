<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test/{id}', function (Request $request, $id) {
        return \App\Models\tests::where("id",$id)->with('questions')->get()[0];
});

Route::get('/tests/search/{word}', function (Request $request, $word) {
        return \App\Models\tests::where("title", "LIKE", '%'.$word.'%')->get();
});


