<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class slider extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slider')->insert([
            'title' => "KENDİNİ SINA",
            'description' => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labor.",
            'image' => "slider.jpg",
            'iframe' => '',
            'link' => "#",
        ]);
        DB::table('slider')->insert([
            'title' => "KENDİNİ SINA",
            'description' => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labor.",
            'image' => '',
            'iframe' => '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/MkNeIUgNPQ8?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            'link' => "#",
        ]);
        DB::table('slider')->insert([
            'title' => "KENDİNİ SINA",
            'description' => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labor.",
            'image' => "slider.jpg",
            'iframe' => '',
            'link' => "#",
        ]);
    }
}
