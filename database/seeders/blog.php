<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class blog extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->insert([
            'title' => "Görev",
            'sub-title' => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy tempor.",
            'description' => "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.",
            'promotion-text' => "test",
            'image' => "shoes.png",
            'number' => "01",
            'bg-text' => "DAYANIKLILIK",
        ]);
    }
}
