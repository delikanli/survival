<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class tests extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $datas = array(
            [
                "title" => 'Sürtünmeyle Ateş Yakma',
                "description" => 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem.',
                "image" => 'test.jpg',
                "tag" => 'anahtar yetenekler',
            ],
            [
                "title" => 'Kendine Sığınak Yap',
                "description" => 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem.',
                "image" => 'test2.jpg',
                "tag" => 'anahtar yetenekler',
            ],
            [
                "title" => 'Ormanda Kendine Bir Öğün Hazırla',
                "description" => 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem.',
                "image" => 'test3.jpg',
                "tag" => 'anahtar yetenekler',
            ],
        );

        foreach ($datas as $data){
             DB::table('tests')->insert([
                'title' => $data['title'],
                'description' => $data['description'],
                'image' => $data['image'],
                'tag' => $data['tag'],
            ]);
             $testId = DB::getPdo()->lastInsertId();
             $answers = array('a', 'b','c', 'd','e');

             for ($i = 0; $i <= 9; $i++){
                 $test = DB::table('questions')->insert([
                     'testId' => $testId,
                     'title' => "Quis autem vel eum iure reprehenderit qui in ea voluptate.",
                     'a' => "A Şıkkı",
                     'b' => "B Şıkkı",
                     'c' => "C Şıkkı",
                     'd' => "D Şıkkı",
                     'e' => "E Şıkkı",
                     'answer' => $answers[array_rand($answers,1)],
                 ]);
             }
        }

    }
}
