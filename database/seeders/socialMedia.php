<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class socialMedia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social-media')->insert([
            'title' => "facebook",
            'icon' => "fab fa-facebook-f",
            'url' => "#",
            'rank' => "1",
        ]);
        DB::table('social-media')->insert([
            'title' => "twitter",
            'icon' => "fab fa-twitter",
            'url' => "#",
            'rank' => "2",
        ]);
        DB::table('social-media')->insert([
            'title' => "youtube",
            'icon' => "fab fa-youtube",
            'url' => "#",
            'rank' => "3",
        ]);
    }
}
