<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Survival</title>


    <link href="https://fonts.googleapis.com/css2?family=Teko:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/owlcarousel/owl.theme.default.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <input type="hidden" id="csrf" value="{{ csrf_token() }}">

</head>

<body>

<div class="menus">
    <div class="container-fluid">
        <div class="row d-flex align-items-center">
            <div class="col-8 col-lg-4 logo d-flex justify-content-center">
                <img src="assets/images/logo.svg" alt="">
            </div>
            <div class="col-4 col-lg-8 d-flex justify-content-end">
                <div class="d-flex align-items-center justify-content-center gap-5 menu">
                    <div>Rakiplerimiz</div>
                    <div>Profesyoneller İçin</div>
                    <div>İletişim</div>
                    <div class="languages">
                        TR
                    </div>
                    <div>
                        <img src="assets/images/Login.svg" alt="">
                    </div>
                    <div class="search d-flex align-items-center justify-content-center">
                        <img src="assets/images/ico_search.svg" alt="">
                        <div class="search-content">
                            <input type="text" placeholder="Test Ara" id="filter_users"/>
                            <ul id="users-list">

                            </ul>
                        </div>
                    </div>

                </div>
                <div class="hamburger">
                    <label for="check">
                        <input type="checkbox" id="check"/>
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                </div>
            </div>

        </div>
    </div>
</div>

<section class="slider " data-aos="fade-in">
    <div class="container-fluid">
        <div class="row">
            <div data-aos="fade-in" class="col-12 col-xl-4 d-flex justify-content-end p-0 slider-left-parent">
                <div class="slider-left slider-padding">
                    <div class="rotate">Zorlu koşullarda nasılsın?</div>

                    <span>KENDİNİ SINA</span>
                    <p>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labor.
                    </p>
                    <div class="slider-left-social d-flex align-items-center gap-4 justify-content-end flex-column">
                        @foreach($socialMedia as $social)
                            <div>
                                <a href="{{ $social->url }}">
                                    <i class="{{ $social->icon }}"></i>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div data-aos="fade-in" class="col-12 col-xl-8 p-0">

                <div class="homepageSlider owl-carousel owl-theme">
                    @foreach($slider as $slide)
                        <div class="slider-right">
                            @if($slide->iframe != "")
                                {!! $slide->iframe !!}
                            @else
                                <img class="img-fluid" src="assets/images/{{ $slide->image }}" alt="">
                            @endif
                            <div class="nav">
                                <a class="right">
                                    <img src="assets/images/arrow-r.svg" alt="">
                                </a>
                                <a class="left">
                                    <img src="assets/images/ico_arrow.svg" alt="">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content" data-aos="fade-in">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-12 col-xl-8 ">
                <h2>
                    Popüler testler
                </h2>
                @foreach($tests as $test)
                    <div data-aos="fade-in" class="tests row d-flex align-items-center justify-content-center">
                        <div class="col-12 col-md-6 d-flex justify-content-center">
                            <img src="assets/images/{{ $test->image }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-md-6 d-flex flex-column justify-content-center p-5">
                            <div class="tag">{{ $test->tag }}</div>
                            <div class="title">{{ $test->title }}</div>
                            <p>
                                {{ $test->description }}
                            </p>
                            <button id="test" data-id="{{ $test->id }}" class="button d-flex align-items-center ">
                                <img src="assets/images/plus.svg" alt="">
                                SINANMAK İÇİN TIKLA
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>
            <div data-aos="fade-in" class="col-12 col-xl-4 d-flex justify-content-center align-items-center">
                <div class="post d-flex justify-content-between gap-3 flex-column">
                        <span>
                            <div class="number">01</div>
                            <div class="promotion">Test</div>
                            {{ $blog[0]->{"bg-text"} }}
                        </span>
                    <img src="assets/images/{{ $blog[0]->image }}" alt="">
                    <h3>{{ $blog[0]->title }}</h3>
                    <div class="title">
                        {{ $blog[0]->{"sub-title"} }}
                    </div>
                    <p>
                        {{ $blog[0]->description }}
                    </p>
                    <div class="button"><img src="assets/images/plus.svg" alt="">SINANMAK İÇİN TIKLA</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="questions">
    <div class="container">
        <div class="test-content">
            <img id="test-content-img" src="" class="img-fluid" alt="">
            <h3 id="test-content-title">Test Title</h3>
            <p id="test-content-desc">Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem.</p>
        </div>
        <div class="row" id="questions">

        </div>
        <div class="check-test">
            <button class="btn " id="test-button">Sonuçları Gör</button>
        </div>
    </div>
</section>

<section class="result">
    <div  class="container-fluid">
        <div class="row justify-content-end">
            <div data-aos="fade-in" class="col-12 col-xl-4  d-flex  justify-content-center flex-column result-left">
                    <span>
                        test sonucun
                    </span>
                <div>
                    hayatta kalma ihtimalın
                </div>
                <div class="percent">
                    98%
                </div>
                <div class="success">
                    <span>12</span> soruyu <br> doğru bildin
                </div>
                <div class="arrow">
                    <img src="assets/images/ico_art-1.svg" alt="">
                </div>
            </div>
            <div data-aos="fade-in" class="col-12 col-xl-8 result-right d-flex align-items-center justify-content-center" style="background-image: url('assets/images/result.jpg')">
                <div class="share">
                    <div class="title">
                        sonuçlarını
                        <br>
                        sosyal medyada paylaş!
                    </div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consetetur diam nonumy tempor.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="footer">
    <div class="container-fluid">
        <div class="row footer-bg justify-content-start align-items-start">
            <div class="col-col-12 col-md-12 col-lg-3 d-flex align-items-center justify-content-center footer-logo">
                <img src="assets/images/footer-logo.svg" class="img-fluid" alt="">
            </div>
            <div class="col-12 col-md-4 col-lg-3">

                <div class="footer-links">
                    <div class="title">
                        TESTLER
                    </div>
                    <div>Endurance</div>
                    <div>Survival</div>
                    <div>Key skills</div>
                    <div>Survival kit</div>
                    <div>Shelter making</div>
                    <div>Food challenges</div>
                </div>
                <div class="social d-flex align-items-center gap-4">
                    @foreach($socialMedia as $social)
                        <div>
                            <a href="{{ $social->url }}">
                                <i class="{{ $social->icon }}"></i>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">

                <div class="footer-links">
                    <div class="title">
                        TAVSİYELER
                    </div>
                    <div>Mountain survival</div>
                    <div>Forest challenges</div>
                    <div>How to make your shelter</div>
                    <div>What to eat in forest</div>
                    <div>How to stay safe</div>
                    <div>What to wear</div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">

                <div class="footer-links">
                    <div class="title">
                        GALERİ
                    </div>
                    <div>Foto gallery</div>
                    <div>Video gallery</div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/font-awesome/all.min.js"></script>
<script src="assets/owlcarousel/owl.carousel.min.js"></script>
<script src="assets/steps/jquery.steps.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="assets/js/script.js"></script>

</body>

</html>
