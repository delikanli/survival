<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\blogs;
use App\Models\tests;
use App\Models\slider;
use App\Models\socialMedia;

class homepage extends Controller
{
    public function index(){


        return view('homepage')
            ->with('blog', blogs::get())
            ->with('tests',tests::all())
            ->with('slider',slider::all())
            ->with('socialMedia',socialMedia::all());
    }
}
