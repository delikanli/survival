<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tests extends Model
{
    use HasFactory;

    //protected $with = ['questions'];

    public function questions()
    {
        return $this->hasMany(questions::class,'testId', "id");
    }
}
