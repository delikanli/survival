$(document).ready(function(){

    var owl = $(".homepageSlider");

    owl.owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        mouseDrag: false,
        nav: false,
        animateOut: 'fadeOut',

    });
    let owlNumber = 0;
    $('.homepageSlider .owl-dots .owl-dot').each(function (){
        owlNumber = parseInt(owlNumber) + 1;
        $(this).find('span').html("0" + owlNumber);
    });

    $(".right").click(function(){
        owl.trigger('next.owl.carousel');
    });
    $(".left").click(function(){
        owl.trigger('prev.owl.carousel', [300]);
    });


    $('#check').on('change',function () {
        $('.menu').toggleClass('active')
        $('.hamburger').toggleClass('active')
    })


    AOS.init();

    ul = document.getElementById("users-list");

    var render_lists = function(lists, ids){
        var li = "";
        for(index in lists){
            li += "<li data-id='" + ids[index] + "'>" + lists[index] + "</li>";
        }
        ul.innerHTML = li;
        $('#users-list li').each(function () {
            $(this).click(function () {
                getTests($(this).data("id"));
                $('.menu').removeClass('active');
                $('.hamburger').removeClass('active');
            })
        })
    }


// lets filters it
    input = document.getElementById('filter_users');

    var token = $('#csrf').val();
    var filterUsers = function(event){
        var filtered_items = [];
        var filtered_ids = [];
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'GET',
            url: "api/tests/search/" + input.value.toLowerCase(),
            dataType: 'html',
            success: function(data) {
                var data = JSON.parse(data);
                data.forEach(function (item) {
                    filtered_items.push(item.title);
                    filtered_ids.push(item.id);
                })
                render_lists(filtered_items,filtered_ids);
            }
        });
    }

    input.addEventListener('input', filterUsers);


    $(".search").hover(function () {
       $(this).find(".search-content").addClass("active");
    });


    $(document).mouseup(function(e)
    {
        var container = $(".search");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.find(".search-content").removeClass("active");
        }
    });


    function printTest(question){
        return `
            <div class="col-12">
                <div class="title">${ question.title }</div>
                <div class="options">
                    <ul>
                        <li><input type="radio" name="soru-${ question.id }" id="cevap-${ question.id }-a" value="a"><label for="cevap-${ question.id }-a">${ question.a }</label></li>
                        <li><input type="radio" name="soru-${ question.id }" id="cevap-${ question.id }-b" value="b"><label for="cevap-${ question.id }-b">${ question.b }</label></li>
                        <li><input type="radio" name="soru-${ question.id }" id="cevap-${ question.id }-c" value="c"><label for="cevap-${ question.id }-c">${ question.c }</label></li>
                        <li><input type="radio" name="soru-${ question.id }" id="cevap-${ question.id }-d" value="d"><label for="cevap-${ question.id }-d">${ question.d }</label></li>
                        <li><input type="radio" name="soru-${ question.id }" id="cevap-${ question.id }-e" value="e"><label for="cevap-${ question.id }-e">${ question.e }</label></li>
                    </ul>
                </div>
            </div>`;
    }

    $('[id=test]').each(function () {
        $(this).on("click",function () {
            id = $(this).data('id');
            getTests(id)
        })
    })



    $("#test-button").click(function () {

        let id = $(this).data("id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'GET',
            url: "api/test/" + id,
            dataType: 'html',
            success: function(data) {
                var data = JSON.parse(data);
                var error = false;

                var total_question = data.questions.length;
                var success = 0;
                data.questions.forEach(function (question) {
                    if (error) return false;
                    let answer = $("input[name='soru-" + question.id + "']:checked").val();
                    if (answer === undefined){
                        alert("Lütfen tüm seçenekleri işretleyiniz.");
                        error = true;

                    }
                    if (answer === question.answer){
                        success++;
                    }
                });

                if (!error){
                    var percent = (success / total_question) * 100;

                    $('.percent').html(percent + "%");
                    $('.success span').html(success);
                    $('.questions ').hide();
                    $('.result ').show();
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".result").offset().top
                    }, 0);

                    setCookie("soru" + id, '{"percent":' + percent + ',"success":"' + success + '"}',30)

                }

            }
        });
    })


    function getTests(id) {
        if (readCookie('soru' + id)){
            let cookie = readCookie('soru' + id);
            value = JSON.parse(cookie);
            $('.percent').html(value.percent + "%");
            $('.success span').html(value.success);
            $('.questions ').hide();
            $('.result ').show();
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".result").offset().top
            }, 0);
        }else {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'GET',
                url: "api/test/" + id,
                dataType: 'html',
                success: function(data) {
                    var data = JSON.parse(data);
                    $('#test-content-img').attr('src', '/assets/images/' + data.image);
                    $('#test-content-title').html(data.title);
                    $('#test-content-desc').html(data.description);
                    $('#test-button').attr("data-id",data.id);
                    $('#questions').html("");
                    data.questions.forEach(function (item) {
                        $('#questions').append(printTest(item));
                    })
                    $('.questions').show();
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".questions").offset().top
                    }, 100);
                }
            });
        }
    }


    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

});
